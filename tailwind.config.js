module.exports = {
    content: [
        "./index.html",
        "./src/**/*.{vue,js}"
    ],
    theme: {
        fontFamily: {
            'sans': ['Montserrat', 'sans-serif']
        },
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            'white': '#FFFFFF',
            'space-gray': {
                50: '#EEEDEA',
                500: '#868686',
                900: '#141312'
            },
            'occo': {
                'dark': '#131210'
            },
            'red': {
                400: '#E40038',
                500: '#f8173a'
            },
            'violet': '#BF4DD4',
            'orange': '#EF6000'
        },
        linearBorderGradients: {
            directions: {
                't': 'to top',
                'tr': 'to top right',
                'r': 'to right',
                'br': 'to bottom right',
                'b': 'to bottom',
                'bl': 'to bottom left',
                'l': 'to left',
                'tl': 'to top left',

            },
            colors: {
                'violet-red-orange': ['#BF4DD4', '#E40038', '#EF6000']
            },
            background: {
                'dark-button': '#131210'
            },
            borders: {
                '1': '1px',
                '2': '2px',
                '4': '4px'
            }
        },
        extend: {
            gridTemplateColumns: {
                'footer': 'auto 1fr auto'
            }
        },
    },
    plugins: [
        require('tailwindcss-border-gradient-radius'),
        require('@tailwindcss/typography'),
    ],
}
