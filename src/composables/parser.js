import { h, compile, defineComponent } from 'vue/dist/vue.esm-bundler.js'

export default defineComponent({
	setup(props, { slots }) {
      return () =>  h('div', { class: 'prose' } , slots.default().map(vnode => h(compile(vnode.children))));
    }    
})