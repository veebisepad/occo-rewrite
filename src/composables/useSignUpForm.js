import { reactive, ref } from 'vue'
import useVuelidate from '@vuelidate/core'
import { required, email } from '@vuelidate/validators'
import axios from 'axios'

export default function useSignUpForm() {
	const form = reactive({
		countryCode: '',
		email: '',
		languageCode: '',
		name: '',
		phoneCode: '',
		phoneNumber: '',
		subscribed: false,
		termsAccepted: true
	})

	const rules = {
		countryCode: { required, $autoDirty: true },
		email: { required, email, $autoDirty: true },
		languageCode: { required, $autoDirty: true },
		name: { required, $autoDirty: true },
		phoneCode: { required, $autoDirty: true },
		phoneNumber: { required, $autoDirty: true },
		subscribed: { required, $autoDirty: true },
		termsAccepted: { required, $autoDirty: true }
	}

	const formStep = ref(1)
	const v$ = useVuelidate(rules, form)
	const responseStatus = ref(null)

	const reset = (form) => {
		form.countryCode = '',
		form.email = '',
		form.languageCode = '',
		form.name = '',
		form.phoneCode = '',
		form.phoneNumber = '',
		form.subscribed = false,
		form.termsAccepted = true,
		responseStatus.value = null
	}

	const submitFirstStep = (form) => {
		axios.post('https://alpha-apigateway.occo.digital/users/auth/register/requests/mobile-verification', form)
		.catch(error => responseStatus.value = error)
	}

	const submitSecondStep = (form, value) => {
		const countryCode = form.countryCode
		const email = form.email
		const languageCode = form.languageCode
		const name = form.name
		const phoneCode = form.phoneCode
		const phoneNumber = form.phoneNumber
		const subscribed = form.subscribed
		const termsAccepted = form.termsAccepted
		const verificationCode = value

		axios.post('https://alpha-apigateway.occo.digital/users/auth/register/requests/landing', {
			countryCode: countryCode,
			email: email,
			languageCode: languageCode,
			name: name,
			phoneCode: phoneCode,
			phoneNumber: phoneNumber,
			subscribed: subscribed,
			termsAccepted: termsAccepted,
			verificationCode: verificationCode
		})
	}

	return {
		form,
		v$,
		reset,
		formStep,
		submitFirstStep,
		submitSecondStep,
		responseStatus
	}
}