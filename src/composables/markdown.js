import { marked } from 'marked'
import { h, defineComponent, compile } from 'vue/dist/vue.esm-bundler'
import Parser from '@/composables/parser.js'

export default defineComponent({
	setup(props, { slots }) {
		return () => h(Parser, () => marked(slots.default().reduce((acc, vnode) => acc + vnode.children, '')))
	}
})
