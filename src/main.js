import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createHead } from '@vueuse/head'
import App from '@/App.vue'
import '@/index.css'
import router from '@/router/index.js'
import VueSmoothScroll from 'vue3-smooth-scroll'

createApp(App)
.use(router)
.use(createPinia())
.use(createHead())
.use(VueSmoothScroll)
.mount('#app')
