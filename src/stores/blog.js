import { defineStore } from 'pinia'
import { format, parse } from 'date-fns'

export const useBlogStore = defineStore('blog', {
	state: () => {
		return {
			blogs: {},
			post: {}
		}
	},
	getters: {
		published_at: (state) => state.post.published_at ? format(parse(state.post.published_at, "yyyy-MM-dd'T'HH:mm:ss.SSSxxx", new Date()), 'dd.MM.yyyy') : ''
	},
	actions: {
		async fetchBlogPosts() {
			try {
				const response = await fetch('https://cms.occo.digital/blogs').then(response => response.json())
				this.blogs = response
			} catch (error) {
				console.log(error)
			}
		},
		async fetchSinglePost(id) {
			try {
				const response = await fetch(`https://cms.occo.digital/blogs/${id}`).then(response => response.json())
				this.post = response
			} catch (error) {
				console.log(error)
			}
		}
	}
})