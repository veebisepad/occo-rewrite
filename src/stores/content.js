import { defineStore } from 'pinia'

export const useContentStore = defineStore('content', {
    state: () => {
        return {
            header: {},
            testimonials: {},
            mission: {},
            joinTheTop: {},
            team: {},
            subscribe: {},
            registerModal: {},
            content: {},
            footer: {}
        }
    },
    actions: {
        async fetchContent() {
            try {
                const response = await fetch('https://cms.occo.digital/landing-page').then(response => response.json())
                this.header = response.header
                this.testimonials = response.testimonial
                this.mission = response.mission
                this.joinTheTop = response.joinTheTop
                this.team = response.team
                this.subscribe = response.subscribe
                this.registerModal = response.registerModal
                this.footer = response.footer
            } catch (error) {
                console.log(error)
            }
        }
    },
})
