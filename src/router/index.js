import { createRouter, createWebHistory } from "vue-router";

import BaseLayout from '@/layouts/BaseLayout.vue'
import LightLayout from '@/layouts/LightLayout.vue'

import Home from '@/pages/Home.vue'
import BlogList from '@/pages/BlogList.vue'
import BlogPost from '@/pages/BlogPost.vue'
import Contact from '@/pages/Contact.vue'
import TermsAndConditions from '@/pages/TermsAndConditions.vue'

const routes = [
	{
		path: '/',
		name: 'Home',
		component: BaseLayout,
		children: [
			{
				path: '',
				name: 'Home',
				component: Home
			}
		]
	},
	{
		path: '/blog',
		name: 'Blog',
		component: BaseLayout,
		children: [
			{
				path: '',
				name: 'BlogList',
				component: BlogList,
			}
		]
	},
	{
		path: '/blog/:id',
		name: 'BlogPost',
		component: LightLayout,
		children: [
			{
				path: '',
				name: 'SingleArticle',
				component: BlogPost
			}
		]
	},
	{
		path: '/contact',
		name: 'Contact',
		component: BaseLayout,
		children: [
			{
				path: '',
				name: 'Contact',
				component: Contact
			}
		]
	},
	{
		path: '/terms-and-conditions',
		name: 'Terms',
		component: LightLayout,
		children: [
			{
				path: '',
				name: 'Terms',
				component: TermsAndConditions
			}
		]
	}
]

const router = createRouter({
	history: createWebHistory(),
	routes,
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		}

		if (to.hash) {
			console.log(to.hash)
			return { el: to.hash, behavior: 'smooth' }
		}
		return { top: 0 }
	}
})

export default router