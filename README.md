# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Developement

- [Figma](https://www.figma.com/file/bflIImtGUM4q8wriQgffIW/occo.digital-landing?node-id=0%3A1)

#### API

- [Content API](https://cms.occo.digital/landing-page)
- [Blog Posts API](https://cms.occo.digital/blogs/)
- [Article API](https://cms.occo.digital/blogs/5)

